// filter

const Array4 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

const geradenZahlen = Array4.filter(function(element) {
  return element % 2 === 0;
});

console.log(geradenZahlen); // Ausgabe: [2, 4, 6, 8, 10]

//map()
const Array1 = [1, 2, 3, 4, 5];

const verdoppeltesArray = Array1.map(function(element) {
  return element * 2;
});

console.log(verdoppeltesArray); // Ausgabe: [2, 4, 6, 8, 10]

//reduce()
const ursprünglichesArray = [1, 2, 3, 4, 5];

const summe = ursprünglichesArray.reduce(function(akkumulator, element) {
  return akkumulator + element;
}, 0);

console.log(summe); // Ausgabe: 15

const addThreeNumbers = curriedMathOperation('add')(3);
console.log(addThreeNumbers(5)(7));  // Sollte 15 ergeben

const multiplyByTwo = curriedMathOperation('multiply')(2);
console.log(multiplyByTwo(4)(6));    // Sollte 48 ergeben