function unary(fn) {
  return function(value) {
    return fn(value);
  };
}

var numbers = ["1", "2", "3"];
var parsedNumbers = numbers.map(unary(parseInt));
console.log(parsedNumbers); // [1, 2, 3]
