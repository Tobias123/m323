function getHelloWorldFunction() {
    return function() {
      console.log("Hello World");
    };
  }
  
  const helloWorldFunction = getHelloWorldFunction();
  helloWorldFunction(); // Gibt "Hello World" aus
  

  function createCounter() {
    let count = 0;
    return function() {
      return count++;
    };
  }
  
  const counter = createCounter();
  console.log(counter()); // Gibt 1 aus
  console.log(counter()); // Gibt 2 aus
  console.log(counter()); // Gibt 3 aus
  

  function createAdder() {
    let total = 0;
    return function(number) {
      total += number;
      return total;
    };
  }
  
  const addNumbers = createAdder();
  console.log(addNumbers(5)); // Gibt 5 aus
  console.log(addNumbers(10)); // Gibt 15 aus
  console.log(addNumbers(3)); // Gibt 18 aus
  

  function createMultiplier(initialValue) {
    return function(factor) {
      return initialValue * factor;
    };
  }
  
  const multiplier = createMultiplier(10);
  console.log(multiplier(5)); // Gibt 50 aus
  console.log(multiplier(2)); // Gibt 20 aus

  /*
  Currying
  berechnungen von Fakultäten oder Fibonacci 
  um eine schleife zu machen
  */


  //AUFGABE 4

  function printNumbers(start, end) {
    if (start > end) {
      return;
    }
    console.log(start);
    printNumbers(start + 1, end);
  }
  
  //printNumbers(1, 100);
  //printNumbers(50, 75); 
  printNumbers(5, 10); 

  
