function multiply(multiplikation, start, ende) {
    const result = [];
    for (let i = start; i <= ende; i++) {
      result.push(i * multiplikation);
    }
    return result;
  }
  console.log(multiply(10, 0, 3)); 
  

  function multiplyCurry(multiplikation) {
    return function(start, ende) {
      return multiply(multiplikation, start, ende);
    };
  }
  
  const rowBy10 = multiplyCurry(10);
  console.log(rowBy10(0, 3));
  