function min(...arr) {
  return arr.reduce((x, y) => {
    if (x > y) {
      return y;
    }
    return x;
  });
}

function max(...arr) {
  return arr.reduce((x, y) => {
    if (x < y) {
      return y;
    }
    return x;
  });
}

function average(...arr) {
  return arr.reduce((x, y) => x + (y || 0)) / arr.length;
}

var grades = [1.2, 3.5, 5, 3, 4.75, , 6, 5.25];

console.log("Tiefste Note" , min(...grades));
console.log("Höchste Note" , max(...grades));
console.log("Durschnit der Noten" , average(...grades));