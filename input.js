/*
* Beim Funtkionalen Programieren und bei einer Mathematischen Funktion gibt es immer das gleiche
* zurück, wenn man das gleiche als parameter eingibt.
*
*/

// 1. Ein Funktionsargument soll einen Default Wert erhalten wenn der Parameter nicht übergeben wird.

function defaultName(name = "defaultName") {
  console.log(`Hallo ${name}`);
}

defaultName(); // Ausgabe: Hallo, Gast!
defaultName("John"); // Ausgabe: Hallo, John!

//2. Sie möchten wissen wie viele Argumente Sie der Funktion übergeben müssen.

function Argumente1(a, b, c) {
  console.log(`Anzahl der Argumente welche übergeben werden müssen: ${Argumente1.length}`);
}

Argumente1(1, 2);

// 3. Sie möchten wissen wie viele Argumente in die Funktion eingegeben wurden.

function Argumente2(a, b, c) {
    console.log(`Anzahl der übergebenen Argumente: ${arguments.length}`);
  }

  Argumente2(1,5);

// 4. Zeigen Sie mindestens zwei Varianten wie sie eine beliebige Anzahl Argumente in Ihrer Funktion verarbeiten können.

function vieleArgumente(...args) {
    console.log(`test ${args}`); 
  }
  
vieleArgumente(1,2,2,2,2)
vieleArgumente(3)

function vieleArgumente2() {
    console.log(arguments); 
  }

vieleArgumente2(2,2,2,2,3);