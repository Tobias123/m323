(async () => {
  const response = await fetch("https://worldcupjson.net/matches");
  const data = await response.json();

  const result = data.reduce(
    (acc, match) => {
      const homeTeam = match.home_team;
      const awayTeam = match.away_team;
      const totalGoals = homeTeam.goals + awayTeam.goals;

      // Count goals by team
      acc.goalsByTeam[homeTeam.name] = (acc.goalsByTeam[homeTeam.name] || 0) + homeTeam.goals;
      acc.goalsByTeam[awayTeam.name] =
        (acc.goalsByTeam[awayTeam.name] || 0) + awayTeam.goals;

      // Update game with most goals
      if (
        !acc.gameWithMostGoals ||
        totalGoals > acc.gameWithMostGoals.goals
      ) {
        acc.gameWithMostGoals = {
          matchId: match.id,
          goals: totalGoals,
        };
      }

      // Count goals by stadium
      acc.goalsByStadium[match.venue] =
        (acc.goalsByStadium[match.venue] || 0) + totalGoals;

      return acc;
    },
    { goalsByTeam: {}, goalsByStadium: {}, gameWithMostGoals: null }
  );

  const sortedteams = Object.entries(result.goalsByTeam).sort(
    (a, b) => b[1] - a[1]
  );
  const top5 = sortedteams.slice(0, 5);
  const flop5 = sortedteams.slice(-5);

  console.log("Top 5:");
  top5.forEach(([team, goals]) => {
    console.log(`${team}: ${goals} Goals`);
  });

  console.log("\nFlop 5:");
  flop5.forEach(([team, goals]) => {
    console.log(`${team}: ${goals} Goals`);
  });

  console.log("\nGame with most goals:");
  console.log(`ID: ${result.gameWithMostGoals.matchId}`);
  console.log(`Goals: ${result.gameWithMostGoals.goals}`);

  const sortedStadiums = Object.entries(result.goalsByStadium).sort(
    (a, b) => b[1] - a[1]
  );
  const stadiumWithMostGoals = sortedStadiums[0];

  console.log("\nStadium with most goals:");
  console.log(`Stadium: ${stadiumWithMostGoals[0]}`);
  console.log(`Goals: ${stadiumWithMostGoals[1]}`);
})();
