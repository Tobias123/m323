function words(str) {
  return String(str)
    .toLowerCase()
    .split(/\s|\b/)
    .filter(function alpha(v) {
      return /^[\w]+$/.test(v);
    });
}

function unique(list) {
  var uniqList = [];

  for (let v of list) {
    // value not yet in the new list?
    if (uniqList.indexOf(v) === -1) {
      uniqList.push(v);
    }
  }

  return uniqList;
}

fetch("https://bible-api.com/?random=verse")
  .then(x => x.json())
  .then(x => x["text"])
  .then(customText => {
    var customWordsFound = words(customText);
    var customUniqueWords = unique(customWordsFound);
    console.log(customUniqueWords);
  });

