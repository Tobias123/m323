function add() {}
function sub() {
  return;
}
function tree() {
  return undefined;
}

console.log(add());
console.log(sub());
console.log(tree());
// Immer undefined

function calc(x) {
  if (x < 10) return x + 10;
  var y = x / 2;
  if (y < 5) {
    if (x % 2 ** 0) return x;
  }
  if (y > 10) return y;
  return x;
}
console.log(calc(5)); //15
console.log(calc(20)); //10

function calc(x) {
  var result;

  if (x < 10) {
    result = x + 10;
  } else {
    var y = x / 2;

    if (y < 5) {
      if (x % 2 === 0) {
        result = x;
      } else {
        result = y;
      }
    } else {
      result = y;
    }
  }

  return result;
}

/*
pure function
sind funktionen welche nichts verändern.

*/

function sum(listOfNumbers) {
  var sum = 0;
  var copy = listOfNumbers.slice();
  for (let i = 0; i < copy.length; i++) {
      if (!copy[i]) 
      copy[i] = 0;

      sum = sum + copy[i];
      
  }
  return sum;
}

var numbers = [ 1, , 3, 9, 27, 64 ];

sum( numbers );  
console.log(sum( numbers ));


function sum(listOfNumbers) {
  return listOfNumbers.reduce((acc, num) => {
      const value = num || 0;
      return acc + value;
  }, 0);
}


function outerFunction(param) {
  var variable = "variable";

  function innerFunction() {
    console.log(variable + " und " + param);
  }

  return innerFunction;
}

var closureExample = outerFunction("Parameter");

closureExample();
