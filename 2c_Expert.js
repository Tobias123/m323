const fs = require('fs')

function loadData(file){
    let index = 0
    let data = fs.readFileSync(file, "utf8").split("\r\n");
    return (numberOfLines) => {
        var line = data.slice(index, index += numberOfLines)
        if (line.length > 0) {
            return line
        }
        return "no more data"
    }
}

var dataLoader = loadData("data.txt")
var page1 = dataLoader(5);
console.log(page1);
var page2 = dataLoader(5);
console.log(page2);
var page3 = dataLoader(5);
console.log(page3);