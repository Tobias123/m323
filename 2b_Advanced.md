Aufgabe 1: Erklärung des Codes

Der Code verwendet die map-Funktion auf dem Array ["1", "2", "3"]. Das Ziel ist es vermutlich, die Elemente des Arrays durch die Funktion parseInt zu transformieren von Strings zu Integer.

Aufgabe 2: Warum funktioniert es nicht?

Das Problem liegt darin, dass die map-Funktion drei Argumente akzeptiert: das aktuelle Element, den Index und das Array selbst.
Die parseInt-Funktion nimmt jedoch zwei Argumente entgegen: den zu parsenden String und die Basis des Zahlensystems.

Wenn map die Funktion für jedes Element aufruft, übergibt es automatisch drei Argumente: parseInt(value, index, array).
Das führt zu unerwarteten Ergebnissen, da parseInt den Index als Basis interpretiert, was zu unerwünschten Konvertierungen führt.

Aufgabe 3: Funktionierende Umformung

Um den Code korrekt auszuführen, kann man eine Funktion verwenden, die nur das erste Argument von parseInt nutzt. Das lässt sich beispielsweise mit einer anonymen Funktion erreichen:

["1", "2", "3"].map(function(value) {
  return parseInt(value);
});

Alternativ kann man auch die Arrow-Funktion verwenden:

["1", "2", "3"].map(value => parseInt(value));

Beide Ansätze stellen sicher, dass nur das erste Argument von parseInt genutzt wird, und somit die Umwandlung der Zeichenketten in Ganzzahlen korrekt erfolgt.
