var adder = function (number1, number2) {
    return number1 + number2;
}

adder(1, 6); // Aufruf der Funktion

// Funktion in Funktion
function forEach(list, fn) {
    for (let v of list) {
        fn( v ); // hier wird die als argument übergebene Funktion aufgerufen
    }
}

forEach( [1,2,3,4,5,6], function each(val){
    console.log( val );
} );


function outerFunction() {
    return function innerFunction(msg){
        return msg.toUpperCase();
    };
}

var myFunction = outerFunction();

myFunction( "gibz" );   

function outerFunction1() {
    return callerFunction(function innerFunction(msg){
        return msg.toUpperCase();
    });
}

function callerFunction(func) {
    return func("gibz");
}

outerFunction1();       

