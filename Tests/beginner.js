function berechneFakultät(n) {
    if (n === 0 || n === 1) {
        return 1;
    } else {
        let fakultät = 1;
        for (let i = 2; i <= n; i++) {
            fakultät *= i;
        }
        return fakultät;
    }
}


function testFakultät() {
    console.log("Testfall 1: Fakultät von 0 sollte 1 sein");
    console.log("Ergebnis:", berechneFakultät(0));
    console.log("Korrekt:", berechneFakultät(0) === 1 ? "Korrekt" : "Falsch");

    console.log("Testfall 2: Fakultät von 5 sollte 120 sein");
    console.log("Ergebnis:", berechneFakultät(5));
    console.log("Korrekt:", berechneFakultät(5) === 120 ? "Korrekt" : "Falsch");

    console.log("Testfall 3: Fakultät von 7 sollte 5040 sein");
    console.log("Ergebnis:", berechneFakultät(7));
    console.log("Korrekt:", berechneFakultät(7) === 5040 ? "Korrekt" : "Falsch");

    console.log("Testfall 4: Fakultät von 10 sollte 3628800 sein");
    console.log("Ergebnis:", berechneFakultät(10));
    console.log("Korrekt:", berechneFakultät(10) === 3628800 ? "Korrekt" : "Falsch");
}

// Testen der Fakultätsfunktion
testFakultät();