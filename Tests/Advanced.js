// Sortierfunktion
function sortiereZahlen(liste) {
  return liste.slice().sort(function (a, b) {
    return a - b;
  });
}

// Tests
function testSortierfunktion() {
    const testfälle = [
        // Testfall 1: Liste mit gemischten Zahlen
        {
          input: [3.2347, 1, 3454, 1, 25.7, 967, 5],
          expected: [1, 1, 3.2347, 5, 25.7, 967, 3454],
        },
        // Testfall 2: Leere Liste
        {
          input: [],
          expected: [],
        },
        // Testfall 3: Liste mit einer Zahl
        {
          input: [42],
          expected: [42],
        },
        // Testfall 4: Liste mit negativen Zahlen
        {
          input: [-5, -23, -8, -1, -10],
          expected: [-23, -10, -8, -5, -1],
        },
        // Testfall 5: Liste mit leeren Zeichenketten
        {
          input: ["", "", "", ""],
          expected: ["", "", "", ""],
        },
        // Testfall 6: Liste mit Komma Zahlen
        {
            input: [5.2, 23.23, 8, 234, 5.5, 5.4, 5.6],
            expected: [5.2, 5.4, 5.5, 5.6, 8, 23.23, 234],
          },
      ];

  var testfallnummer = 1;
  testfälle.forEach((testfall) => {
    const { input, expected } = testfall;
    const originalInput = input.slice(); 
    const result = sortiereZahlen(input);
    const success =
      JSON.stringify(result) === JSON.stringify(expected)
        ? "Erfolgreich"
        : "Fehlgeschlagen";

    const originalUnchanged =
      JSON.stringify(input) === JSON.stringify(originalInput)
        ? "Ursprüngliche Liste bleibt unverändert"
        : "Ursprüngliche Liste wurde verändert";

        console.log(`Testfall: ${testfallnummer++}, Ergebnis: ${success}, ${originalUnchanged}`);
  });
}


// Tests durchführen
testSortierfunktion();
