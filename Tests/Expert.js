// Fibonacci-Rekursionsfunktion
function calculateFibonacci(n) {
  if (n <= 1) return n;
  return calculateFibonacci(n - 1) + calculateFibonacci(n - 2);
}

// Leistungstests für die Fibonacci-Funktion
function PerformanceTests() {
  const testData = [
    { input: 2, output: 1 },
    { input: 7, output: 13 },
    { input: 8, output: 21 },
    { input: 12, output: 144 },
    { input: 20, output: 6765 },
  ];

  testData.forEach((test) => {
    const startTime = new Date().getTime();
    const result = calculateFibonacci(test.input);
    const endTime = new Date().getTime();
    const duration = endTime - startTime;

    result !== test.output
      ? console.log(`Test fehlgeschlagen für Eingabe ${test.input}. Falsches Ergebnis: ${result}`)
      : console.log(
        `Test erfolgreich für Eingabe ${test.input}. Dauer: ${duration}ms`
        );

    if (duration > 1000) {
      console.log(
        `Warnung: Die Berechnung dauerte länger als 1 Sekunde für Eingabe ${test.input}. Erwägen Sie eine Optimierung der Implementierung.`,
      );
    }
  });
}

PerformanceTests();
