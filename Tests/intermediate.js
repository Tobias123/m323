function istPalindrom(str) {
    const bereinigterString = str.replace(/\s/g, '').toLowerCase();

    const umgekehrterString = bereinigterString.split('').reverse().join('');
    return bereinigterString === umgekehrterString;
}

function testPalindrom() {
    const testfälle = [
        { wort: "Anna", erwartet: true },
        { wort: "Lagerregal", erwartet: true },
        { wort: "Hallo", erwartet: false },
        { wort: "Rentner", erwartet: true },
        { wort: "Waschsalon", erwartet: false }
    ];

    testfälle.forEach(testfall => {
        const ergebnis = istPalindrom(testfall.wort);
        console.log(`Wort: ${testfall.wort}, Erwartet: ${testfall.erwartet}, Ergebnis: ${ergebnis ? 'Korrekt' : 'Falsch'}`);
    });
}

testPalindrom();
